<?php

$menu_args = array(
    //'menu'              => null,              // id, slug, name or object
    'menu_class'        => 'main-navigation',   // class name for ul element
    'menu_id'           => 'navigation-main',   // id for ul element
    'container'         => 'div',               // wrapper element
    'container_class'   => 'navigation-primary',// wrapper class
    //'container_id'      => null,              // wrapper id
    //'fallback_cb'       => null,              // on fail callback
    //'before'            => null,              // text before markup
    //'after'             => null,              // text after markup
    //'link_before'       => null,              // text before text
    //'link_after'        => null,              // text after text
    'echo'              => true,                // false returns the string
    'theme_location'    => Laconst::NAVMENU,    // must be registered
    'depth'             => 0,                   // levels of hyerarchy
    'item_spacing'      => 'preserve'           // preserve the whitespace
    );

wp_nav_menu($menu_args);

