<?php
/** vim: fileencoding=utf-8

/********************************************************************
 *                                                                  *
 *    Copyright © George Stamoulis - 2017 - All Rights Reserved.    *
 *    This file is part of the Lacandona Wordpress plugin.          *
 *    Proprietary and confidential.                                 *
 *                                                                  *
 *    Unauthorized copying of any part of this file                 *
 *    via any medium is strictly prohibited.                        *
 ********************************************************************/

add_theme_support('post-thumbnails');

add_action('wp_enqueue_scripts', 'enqueue_stylesheets');

/**
 * Prints the path to the themes images directory.
 *
 * @param   $subdir Optional subdirectory to append to the path
 */
function images_dir($subdirectory = ''){
    echo(get_template_directory_uri() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . $subdirectory);
}

/**
 * Callback that calls {@see wp_enqueue_style} after the 'wp_enqueue_scripts' hook.
 */
function enqueue_stylesheets(){
    wp_enqueue_style('style', get_stylesheet_uri(), array('dashicons'));
    // TODO create and add a handheld stylesheet.
}

/**
 * Shows posts of 'event' type on home page.
 */
function homepage_query($query) {
  if (is_home() && $query->is_main_query()){
      if(class_exists('Laconst')){
        $query->set('post_type', array(Laconst::EVENT));
      }
  }
  return $query;
}
// XXX: Uncomment the next line to just show events in the homepage.
//add_action( 'pre_get_posts', 'homepage_query' );

/**
 * Makes the sidebar widget-aware.
 */
function widgetize(){
    $widget_areas = array(
        'names' => array(__('Sidebar Left Top', Laconst::TXTDMN), __('Sidebar Right Bottom', Laconst::TXTDMN)),
        'ids'   => array('sidebar-left-top', 'sidebar-right-bottom')
    );
    for($i = 0; $i < count($widget_areas); $i++){
        register_sidebar(array(
            'name' => $widget_areas['names'][$i],
            'id'   => $widget_areas['ids'][$i],
            'description' => __('Resides where the name implies.', Laconst::TXTDMN),
            'before_widget' => '<div>',
            'after_widget'  => '</div>',
            'before_title'  => '<h2 class="rounded widget-title">',
            'after_title'   => '</h2>'
        ));
    }
}
add_action('widgets_init', 'widgetize');

/**
 * Adds navigation support to the theme.
 */
function register_menus(){
    register_nav_menu(Laconst::NAVMENU, __('Main Navigation', Laconst::TXTDMN));
    register_nav_menu(Laconst::SOCMENU, __('Social Links'   , Laconst::TXTDMN));
}
/**
 * First hook available for themes.
 * Triggers just after the fuctions file has been loaded.
 */
add_action('after_setup_theme', 'register_menus');
