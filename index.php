<?php
/** vim: fileencoding=utf-8

/********************************************************************
 *                                                                  *
 *    Copyright © George Stamoulis - 2017 - All Rights Reserved.    *
 *    This file is part of the Lacandona Wordpress plugin.          *
 *    Proprietary and confidential.                                 *
 *                                                                  *
 *    Unauthorized copying of any part of this file                 *
 *    via any medium is strictly prohibited.                        *
 ********************************************************************/

get_header();
?>

<div id="primary" class="site-content">
    <main id="main" class="site-main" role="main">

<?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            $perma = get_permalink();
            printf('<article id="%s" class="post-article">', basename($perma));
            if(has_post_thumbnail($post)){
                printf('<a href="%s">', $perma);
                //the_post_thumbnail( 'thumbnail' );     // Thumbnail (150 x 150 hard cropped)
                //the_post_thumbnail( 'medium' );        // Medium resolution (300 x 300 max height 300px)
                the_post_thumbnail( 'medium_large' );  // Medium Large (added in WP 4.4) resolution (768 x 0 infinite height)
                //the_post_thumbnail( 'large' );         // Large resolution (1024 x 1024 max height 1024px)
                //the_post_thumbnail( 'full' );          // Full resolution (original size uploaded)
                echo('</a>');
            }
            printf('<h2><a href="%s" rel="bookmark" class="post-title" title="Permanent Link to %s">%s</a></h2>', $perma, the_title_attribute(array('echo' => false)), the_title('', '', false));

            //printf('<a class="title-link" href="%s">', the_gurl());
            //the_title('<h3 class="post-title list-title">', '</h3>', true);
            printf('<div class="post-content list-content">%s</div>', apply_filters('the_content', get_the_content(__('More', Laconst::TXTDMN))));
            echo('</article>');
            //var_dump($post);
            //echo('<hr/>');



        } // end while
    }else{
        printf('<div class="post-article post-content"><p>%s</p></article>', __('Sorry, no content found.', Laconst::TXTDMN));
    } // end if
?>
    </main><!-- #main -->
    <?php get_template_part('social');?>
</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
