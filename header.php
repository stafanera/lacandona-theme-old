<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- TODO: manage og:meta for fb twitter and the such. -->
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header id="masthead" class="site-header" role="banner">
        <?php
            get_template_part('logo');
            get_template_part('navigation');
	/*
	 * If a regular post or page, and not the front page, show the featured image.
	 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
	 */
	if ((is_single() ||  is_page()) && has_post_thumbnail(get_queried_object_id())){
		echo '<div class="single-featured-image-header">';
		echo get_the_post_thumbnail( get_queried_object_id(), 'featured-image' );
        echo '</div><!-- .single-featured-image-header -->';
    }else{
        if(!is_404()){
            get_template_part('slideshow');
        }else{
            $path = get_stylesheet_directory_uri() . DIRECTORY_SEPARATOR;
		printf('<div class="single-featured-image-header">
            <audio autoplay preload="auto">
                <source src="%1$ssnd/crickets.mp3"/>
            </audio>
            <img src="%1$simages/404.jpg" class="wp-post-image"/>
            </div><!-- .single-featured-image-header -->', $path);
        }
    }

	?>
	</header><!-- #masthead -->

		<div id="content" class="site-content">
