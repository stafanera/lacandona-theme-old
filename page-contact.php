<?php
/** vim: fileencoding=utf-8

/********************************************************************
 *                                                                  *
 *    Copyright © George Stamoulis - 2017 - All Rights Reserved.    *
 *    This file is part of the Lacandona Wordpress plugin.          *
 *    Proprietary and confidential.                                 *
 *                                                                  *
 *    Unauthorized copying of any part of this file                 *
 *    via any medium is strictly prohibited.                        *
 ********************************************************************/


function get_equation(){
    global $equation;
    $equation = [];
    for($i = 0; $i < 2; $i++){
        $equation[] = rand(0, 20);
    }
    $equation['term'] = sprintf('+ %d = %d', $equation[1], $equation[1] + $equation[0]);
}

function get_response(){
    global $response;
    $response = array('class' => 'hidden', 'message' => '');
    if(isset($_POST['submit'])){
        if($_POST['message'] === '' || $_POST['human'] === ''){
            $response['class'] = 'error';
            $response['message'] = __('Please fill all required fieds.', Laconst::TXTDMN);
            return;
        }
        if($_POST['human'] !== $_POST['correct']){
            $response['class'] = 'error';
            $response['message'] = __('Please practice your math.', Laconst::TXTDMN);
            return;
        }else{
            $ret_val = send_mail();
            $response['class'] = $ret_val ? 'correct' : 'error';
            $response['message'] = $ret_val ? __('Thank you. Your message has been sent.', Laconst::TXTDMN) : __('Ooops, something went quite wrong.. Please try again later.', Laconst::TXTDMN);
            $_POST = [];
        }
    }
}

function send_mail(){
    //TODO: Create a nicer mail.
    $to = "g.a.stamoulis@gmail.com"; //TODO Change to the correct addresses (comma seperated)
    $subject = "email to lacandona.gr from ${_POST['user']}."; //FIXME $_POST['user'] === '' ? 'unknown'
    $message = $_POST['message'];
    $header = !empty($_POST['email']) ? $_POST['email'] : Null;

    return mail($to,$subject,$message,$header);
}

get_equation();
get_response();
get_header();

?>

<div id="primary" class="site-content">
    <main id="main" class="site-main" role="main">

<?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            $perma = get_permalink();
            printf('<article id="%s" class="post-article %s">', basename($perma), implode(' ',get_post_class()));
            printf('<header><h2><a href="%s" rel="bookmark" class="post-title" title="Permanent Link to %s">%s</a></h2></header>', $perma, the_title_attribute(array('echo' => false)), the_title('', '', false));
            echo('<div>');
            printf('<p class="post-content list-content">%s</p>', apply_filters('the_content', get_the_content(__('More', Laconst::TXTDMN))));
            echo('</br>');

            $form = '<form class="form-colored" method="post">
                <div id="contact-form-response" class="%s">%s</div>
                <p class="form_field"><label for="user">Name:</br><input type="text" id="user" name="user" value="%s"/></label></p>
                <p class="form_field"><label for="email">Email:</br><input type="email" id="email" name="email" value="%s"/></label></p>
                <p class="form_field"><label for="message">Message:<span class="red">*</span></br><textarea type="text" id="message" name="message" required>%s</textarea></label></p>
                <p class="form_field"><label for="human">Human Verification:<span class="red">*</span></br><input type="number" id="human" name="human" min="0" max="20" step="1" style="width:2.5em;" required />%s</label></p>
                <input type="hidden" name="correct" value="%d"/>
                <p class="form_field"><input type="submit" name="submit" value="Send"/></p>
                </form>';
                // verification width.

            printf($form, $response['class'], $response['message'], isset($_POST['user']) ? $_POST['user'] : '', isset($_POST['email']) ? $_POST['email'] : '', isset($_POST['message']) ? $_POST['message'] : '', $equation['term'], $equation[0]);
?>

<?php

            echo('</div>');
            echo('</article>');

        } // end while
    } // end if
?>
    </main><!-- #main -->
    <?php get_template_part('social');?>
</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
