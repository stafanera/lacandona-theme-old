# Lacandona WordPress Theme

This theme is meant to be used alongside the lacandona must use wordpress plugin for adjusting the functionality and presentation of the lacandona web page. Other wordpress themes can be used but they will require some tweaking.

## Features

* product post type template
* link post type archive
* event post type template (not fully implemented)
* double sidebar
* social links
* 404

## Todo
* iminent:
    * translate
    * show category
    * show price
    * color scheme
    * better bigger fonts
* generic product image
* responsive
* translate to greek
* open graph metas

## Contact

* **developer**: Stamoulohta (g.a.stamoulis@gmail.com)
* **staff**:
    * Eleni
    * Nasia
    * Dimitra
    * Niovi

## License
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to:

Free Software Foundation, Inc.
51 Franklin Street, Fifth Floor,
Boston, MA
02110-1301, USA.
