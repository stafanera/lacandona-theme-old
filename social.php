<?php

//TODO put all this in a function.

$span = '<span class="dashicons"></span>';

$menu_args = array(
    //'menu'              => null,              // id, slug, name or object
    'menu_class'        => 'social-links',      // class name for ul element
    'menu_id'           => 'social-links',      // id for ul element
    'container'         => 'div',               // wrapper element
    'container_class'   => 'social-wrapper',    // wrapper class
    //'container_id'      => null,              // wrapper id
    //'fallback_cb'       => null,              // on fail callback
    //'before'            => null,              // text before markup
    //'after'             => null,              // text after markup
    'link_before'       => $span,               // text before text
    //'link_after'        => null,              // text after text
    'echo'              => true,                // false returns the string
    'theme_location'    => Laconst::SOCMENU,    // must be registered
    'depth'             => 0,                   // levels of hyerarchy
    'item_spacing'      => 'preserve'           // preserve the whitespace
    );

wp_nav_menu($menu_args);

