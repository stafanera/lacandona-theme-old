<?php
/** vim: fileencoding=utf-8

/********************************************************************
 *                                                                  *
 *    Copyright © George Stamoulis - 2017 - All Rights Reserved.    *
 *    This file is part of the Lacandona Wordpress plugin.          *
 *    Proprietary and confidential.                                 *
 *                                                                  *
 *    Unauthorized copying of any part of this file                 *
 *    via any medium is strictly prohibited.                        *
 ********************************************************************/

get_header();

?>

<div id="primary" class="site-content">
    <main id="main" class="site-main" role="main">

<?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            $perma = get_permalink();
            printf('<article id="%s" class="post-article %s">', basename($perma), implode(' ',get_post_class()));
            printf('<header><h2><a href="%s" rel="bookmark" class="post-title" title="Permanent Link to %s">%s</a></h2></header>', $perma, the_title_attribute(array('echo' => false)), the_title('', '', false));
            echo('<div id="links_div">');
            printf('<h3 class="post-content list-content">%s</h3>', apply_filters('the_content', get_the_content(__('More', Laconst::TXTDMN))));

            global $wpdb;
            $results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}links", OBJECT);
            //print_r($results);


            $i = 0;
            foreach($results as $link){
                $i++;
                printf('<p class="%s">%s<br><a target="%s" href="%s">%s</a></p>', $i % 2 === 0 ? 'link-dark' : 'link-light', $link->link_description, $link->link_target, $link->link_url, $link->link_name);
            }

            echo('</div><!-- #links_div -->');
            echo('</article>');

        } // end while
    } // end if
?>
    </main><!-- #main -->
    <?php get_template_part('social');?>
</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
