<?php
/** vim: fileencoding=utf-8

/********************************************************************
 *                                                                  *
 *    Copyright © George Stamoulis - 2017 - All Rights Reserved.    *
 *    This file is part of the Lacandona Wordpress plugin.          *
 *    Proprietary and confidential.                                 *
 *                                                                  *
 *    Unauthorized copying of any part of this file                 *
 *    via any medium is strictly prohibited.                        *
 ********************************************************************/

include_once('functions.php');

get_header();
?>

<div id="primary">
    <main id="main" class="site-main" role="main">

<?php
    echo('<div class="placeholder">');
    echo('<h2>Comming Soon!!</h2>');
    echo('<h4>check back soon :)</h4>');
    echo('</div>');
?>
    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
?>
