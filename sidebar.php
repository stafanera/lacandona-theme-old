<aside id="main_sidebar">
    <div id="left_sidebar" class="sidebar">
        <?php
            if(is_active_sidebar('sidebar-left-top')){
                echo('<div class="widget_area" role="complementary">');
                dynamic_sidebar('sidebar-left-top');
                echo('</div>');
            }
        ?>
    </div><!-- #left_sidebar -->
    <div id="right_sidebar" class="sidebar">
        <?php
            if(is_active_sidebar('sidebar-right-bottom')){
                echo('<div class="widget_area" role="complementary">');
                dynamic_sidebar('sidebar-right-bottom');
                echo('</div>');
            }
        ?>
    </div><!-- #right_sidebar -->
</aside>

