<?php
/** vim: fileencoding=utf-8

/********************************************************************
 *                                                                  *
 *    Copyright © George Stamoulis - 2017 - All Rights Reserved.    *
 *    This file is part of the Lacandona Wordpress plugin.          *
 *    Proprietary and confidential.                                 *
 *                                                                  *
 *    Unauthorized copying of any part of this file                 *
 *    via any medium is strictly prohibited.                        *
 ********************************************************************/

get_header();
?>

<div id="primary" class="site-content">
    <main id="main" class="site-main" role="main">

<?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            $perma = get_permalink();
            printf('<article id="%s" class="post-article %s">', basename($perma), implode(' ',get_post_class()));
            printf('<header><h2><a href="%s" rel="bookmark" class="post-title" title="Permanent Link to %s">%s</a></h2></header>', $perma, the_title_attribute(array('echo' => false)), the_title('', '', false));
            printf('<p class="post-content list-content">%s</p>', apply_filters('the_content', get_the_content(__('More', Laconst::TXTDMN))));
            echo('</article>');

            the_post_navigation(array(
                'next_text'     => '<span id="pagination-next" class="pagination">' . __('Next', Laconst::TXTDMN) . '</span>',
                'prev_text'     => '<span id="pagination-prev" class="pagination">' . __('Prev', Laconst::TXTDMN) . '</span>',
                'in_same_term'  => true
                )
            );


        } // end while
    } // end if
?>
    </main><!-- #main -->
    <?php get_template_part('social');?>
</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
