<?php
/** vim: fileencoding=utf-8

/********************************************************************
 *                                                                  *
 *    Copyright © George Stamoulis - 2017 - All Rights Reserved.    *
 *    This file is part of the Lacandona Wordpress plugin.          *
 *    Proprietary and confidential.                                 *
 *                                                                  *
 *    Unauthorized copying of any part of this file                 *
 *    via any medium is strictly prohibited.                        *
 ********************************************************************/

get_header();

?>

<div id="primary" class="site-content">
    <main id="main" class="site-main" role="main">

<?php
    echo('<article id="404">');
    //TODO: Add an image.
    printf('<header><h2>%s</h2></header>', '404');
    echo('<br/>');
    echo('<div>');
    printf('<p class="post-content"><strong>%s</strong>%s</p>', $_SERVER['REQUEST_URI'], __('does not exist in this webpage.', Laconst::TXTDMN));
    echo('</div>');
    echo('</article>');
?>
    </main><!-- #main -->
    <?php get_template_part('social');?>
</div><!-- #primary -->

<?php
get_footer();
